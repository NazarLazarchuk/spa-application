<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use App\ArticleImages;
use App\Http\Resources\ArticleImages as ArticleImagesResource;

class ImageController extends Controller
{
    public function store(Request $request)
    {
       $this->validate($request, [

        'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        $image = $request->file('image');

        $input['imagename'] = time().rand(1, 999).'.'.$image->getClientOriginalExtension();

        $imageObject = new ArticleImages;
        $imageObject->name = $input['imagename'];
        $imageObject->article_id = (int)($request->input('article_id'));
        $destinationPath = public_path('/images');

        $image->move($destinationPath, $input['imagename']);

        if($imageObject->save()){
            return new ArticleImagesResource($imageObject);
        }
        
    }

    public function show($id)
    {
        $image = ArticleImages::findOrFail($id);
        $file = public_path('/images')."/".$image->name;
        $size = getimagesize($file);
        header('Content-Type: '.$size['mime']);
        header('Content-Length: ' . filesize($file));
        readfile($file);
    }

    public function destroy($id)
    {
        $image = ArticleImages::findOrFail($id);
        if($image->delete()){
            unlink(public_path('/images')."/".$image->name);
            return new ArticleImagesResource($image);
        }
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ArticleComment;
use App\Http\Resources\ArticleComment as ArticleCommentResource;

class ArticleCommentController extends Controller
{
    public function index()
    {
       $article_comments = ArticleComment::orderBy('created_at', 'desc')->get();

       return new ArticleCommentResource($article_comments);
    }
    public function store(Request $request)
    {
        $edit_mode = $request->isMethod('put');
        $article_comments =  $edit_mode? 
            ArticleComment::findOrFail($request->comment_id) :
            new ArticleComment;
        if(!$edit_mode){
            $article_comments->author_id = (int)$request->input('author_id');
            $article_comments->article_id = (int)$request->input('article_id');
        }
        $article_comments->id = $request->input('comment_id');
        $article_comments->text = $request->input('text');

        if($article_comments->save()) {
            return new ArticleCommentResource($article_comments);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = ArticleComment::findOrFail($id);
        if($comment->delete()){
            return new ArticleCommentResource($comment);
        }
    }
}
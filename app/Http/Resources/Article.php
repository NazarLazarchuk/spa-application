<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\ArticleImages;
use App\Http\Resources\ArticleImages as ArticleImagesResource;
use App\ArticleComment;
use App\Http\Resources\ArticleImages as ArticleCommentResource;
use App\User;

use Carbon\Carbon;
class Article extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      // return parent::toArray($request);
       setlocale(LC_ALL, 'uk_UA');
       Carbon::setLocale('uk');
       $images = ArticleImages::where('article_id', $this->id)->select('id')->get();
       $comments = ArticleComment::where('article_id', $this->id)->orderBy('created_at', 'desc')->get();
       foreach ($comments as &$comment){
            $comment->created_at_ago = $comment->created_at->timezone(3)->diffForHumans();
            $comment->updated_at_ago = $comment->updated_at->timezone(3)->diffForHumans();
            $comment->author_name = User::findOrFail($comment->author_id)->name;
       }

       return [
           'id' => $this->id,
           'title' => $this->title,
           'body' => $this->body,
           'author_id' => $this->author_id,
           'created_at'=> date('Y-m-d H:i:s', strtotime($this->created_at->timezone(3))),
           'updated_at'=> date('Y-m-d H:i:s', strtotime($this->updated_at->timezone(3))),
           'images' => $images,
           'comments' => $comments
       ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleComment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
        // return [
        //     'text' => $this->text,
        //     'author_id' => $this->author_id,
        //     'article_id' => $this->article_id,
        //     'created_at'=> date('Y-m-d H:i:s', strtotime($this->created_at->timezone(3))),
        //     'updated_at'=> date('Y-m-d H:i:s', strtotime($this->updated_at->timezone(3)))
        // ];
    }
}

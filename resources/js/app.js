require('./bootstrap');
window.Vue = require('vue');


// <--components-->
const Navbar =  require('./components/Navbar.vue');
Vue.component('my-navbar', Navbar);

const Article = require('./components/Article.vue');
Vue.component('my-article', Article);

const Articles = require('./components/Articles.vue');
Vue.component('my-articles', Articles);

const EditArticle = require('./components/EditArticle.vue');
Vue.component('my-edit-article', EditArticle);

const Err404 = require('./components/404.vue');
Vue.component('my-err-404', Err404);

const AddArticle = require('./components/AddArticle.vue');
Vue.component('my-add-article', AddArticle);

const Register = require('./components/Register.vue');
Vue.component('register', Register);

const Login = require('./components/Login.vue');
Vue.component('login', Login);

const UploadImage = require('./components/UploadImage.vue');
Vue.component('my-upload-image', UploadImage);

const Carousel = require('./components/Carousel.vue');
Vue.component('my-carousel', Carousel);

const MainPage = require('./components/MainPage.vue');
Vue.component('my-main-page', MainPage);

const Footer = require('./components/Footer.vue');
Vue.component('my-footer', Footer);

const About = require('./components/About.vue');
Vue.component('my-about', About);

const App = require('./components/App.vue');
Vue.component('app', App);
// <--components-->

// <--router-->
const VueRouter = require('vue-router').default;
Vue.use(VueRouter);


const routes = [
    { path: '/', redirect: '/home' },
    { path: '/home', component: MainPage },
    { path: '/about', component: About },
    { path: '/articles', component: Articles },
    { path: '/article/create', component: AddArticle, props: true},
    { path: '/article/create/:id', component: Err404 },
    { path: '/article/:id', component: Article, props: true },
    { path: '/article/:id/edit', component: EditArticle, props: true },
    { path: '/register', name: 'register', component: Register },
    { path: '/login', name: 'login', component: Login },
    { path: '/upload', name: 'upload', component: UploadImage }
]
 
const router = new VueRouter({
    mode: 'history',
    routes
})
// <--router-->

App.router = Vue.router;
Vue.prototype.$eventBus = new Vue();

const app = new Vue({
    el: '#app',
    router
})

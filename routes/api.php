<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('articles', 'ArticleController@index');

Route::get('article/{id}', 'ArticleController@show');

Route::post('articles', 'ArticleController@store');

Route::put('article/{id}', 'ArticleController@store');

Route::delete('article/{id}', 'ArticleController@destroy');

Route::post('auth/register', 'AuthController@register');

Route::post('auth/login', 'AuthController@login');
Route::group(['middleware' => 'jwt.auth'], function(){
  Route::get('auth/user', 'AuthController@user');
  Route::post('auth/logout', 'AuthController@logout');
});
Route::group(['middleware' => 'jwt.refresh'], function(){
  Route::get('auth/refresh', 'AuthController@refresh');
});
Route::get('username/{id}', 'AuthController@username');

Route::post('image', 'ImageController@store');
Route::get('image/{id}', 'ImageController@show');
Route::delete('image/{id}', 'ImageController@destroy');

Route::get('comments', 'ArticleCommentController@index');
Route::get('comment/{id}', 'ArticleCommentController@show');
Route::post('comments', 'ArticleCommentController@store');
Route::put('comment/{id}', 'ArticleCommentController@store');
Route::delete('comment/{id}', 'ArticleCommentController@destroy');
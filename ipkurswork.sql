-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Окт 07 2018 г., 13:37
-- Версия сервера: 10.1.35-MariaDB
-- Версия PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `ipkurswork`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(10) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `author_id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(199, 7, 'Бігль', 'Відомий герой мультфільмів і коміксів Снупі – ніхто інший, як бігль. Цих собак вивели англійці для полювання на дрібну дичину. Вони невеликі, але дуже милі. Біглі - товариські, добрі та активні. Вони обожнюють гратися з дітьми та дозволяють їм практично все. Собаки породи бігль потребують постійної уваги: вони дуже кмітливі, допитливі, але вперті. Так що дресирування біглика може забрати чимало часу.', '2018-10-07 07:15:32', '2018-10-07 07:15:32'),
(200, 7, 'Лабрадор ретривер', 'Жвавий, життєрадісний, відданий та безмежно добрий – усе це про собаку породи лабрадор ретривер. Цей пес найбільше полюбляє активність та увагу. До того ж лікарями доведено, що лабрадори здатні лікувати депресію.\n\nЛабрадори ретривери завжди готові до прогулянок та безкінечних ігор. Вони терплячі та слухняні, а тому дуже легко та швидко навчаються. Через надмірну гіперактивність та доброту, ці собаки навряд чи стануть хорошими охоронцями вашого дому, але, без сумніву, будуть справжніми друзями вашої дитини.', '2018-10-07 07:18:56', '2018-10-07 07:18:56'),
(201, 7, 'Пудель', 'Одна з найпопулярніших порід собак у всьому світі. Вони наділені особливим шармом та гострим розумом. Ці песики легко піддаються дресируванню та покірно виконують усі команди господаря. А ще, вони дуже веселі. Не існує найбільш радісної собаки, ніж пудель. Невичерпна енергія та позитивний характер роблять цю породу ідеальною для будь-якої родини, головне – не лінитися доглядати за його кучерявою шерстю.', '2018-10-07 07:34:30', '2018-10-07 07:34:30'),
(202, 7, 'Персидська кішка', 'Перські кішки - це одна з перших порід, яку діти починають відрізняти. Вони терплячі, в дитинстві дуже грайливі, що буде цікаво дитині.', '2018-10-07 07:39:30', '2018-10-07 07:39:30'),
(203, 7, 'Британська кішка', 'Плюшеві, пухкі, із смішними щічками - британські котики не можуть не викликати розчулення. Так і хочеться їх обійняти!\n\nАле потрібно пояснити дитині, що кішка все ж не іграшка. Але якщо сім\'я завоює довіру, то тварина буде дозволяти себе гладити і обіймати частіше.', '2018-10-07 07:41:31', '2018-10-07 07:41:31'),
(204, 7, 'Хомячки', 'Хом\'ячки - дійсно милі домашні тварини. Вони не займають багато місця, не вимагають регулярного вигулу і величезних витрат на утримання. \nЇх можна тримати в клітинах поодинці, парами або навіть сім\'ями.', '2018-10-07 07:48:18', '2018-10-07 07:48:18'),
(205, 7, 'ДУЖЕ ТЕРМІНОВО віддам кішечку у добрі руки!', 'Кішці майже 4 місяці. Триколірна, дуже любляча, активна і хороша. \nПривчена до лоточка, оглянута. Разом з нею віддаю лоточок і щедрий запас корму.', '2018-10-07 08:09:33', '2018-10-07 08:09:33'),
(206, 7, 'Віддам гарну, молоду вівчарку в добрі руки', 'Однорічна, неагресивна до людей і дітей вівчарка. Їсть все. Без причини не гавкає як дворові пси. Дуже лагідна і повна любові. Віддаю разом з будою.', '2018-10-07 08:24:21', '2018-10-07 08:24:21'),
(207, 7, 'Віддам. Бекоштовно в добрі руки.', 'Молоденька кіточка спокійна. Привчена на лоток. Буде вам вірним другом. Нема можливості за нею доглядати.', '2018-10-07 08:34:03', '2018-10-07 08:34:03');

-- --------------------------------------------------------

--
-- Структура таблицы `article_comments`
--

CREATE TABLE `article_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `article_images`
--

CREATE TABLE `article_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `article_images`
--

INSERT INTO `article_images` (`id`, `article_id`, `name`, `created_at`, `updated_at`) VALUES
(18, 199, '1538907333.jpg', '2018-10-07 07:15:33', '2018-10-07 07:15:33'),
(20, 199, '1538907354.jpg', '2018-10-07 07:15:54', '2018-10-07 07:15:54'),
(21, 200, '1538907537.jpg', '2018-10-07 07:18:57', '2018-10-07 07:18:57'),
(23, 200, '1538907554.jpg', '2018-10-07 07:19:14', '2018-10-07 07:19:14'),
(24, 201, '1538908471.jpg', '2018-10-07 07:34:31', '2018-10-07 07:34:31'),
(25, 201, '1538908472.jpg', '2018-10-07 07:34:32', '2018-10-07 07:34:32'),
(26, 202, '1538908771.jpg', '2018-10-07 07:39:31', '2018-10-07 07:39:31'),
(27, 202, '1538908772.jpg', '2018-10-07 07:39:32', '2018-10-07 07:39:32'),
(28, 203, '1538908891.jpg', '2018-10-07 07:41:31', '2018-10-07 07:41:31'),
(29, 203, '1538908893.jpg', '2018-10-07 07:41:33', '2018-10-07 07:41:33'),
(30, 204, '1538909299.jpg', '2018-10-07 07:48:19', '2018-10-07 07:48:19'),
(31, 204, '1538909300.jpg', '2018-10-07 07:48:20', '2018-10-07 07:48:20'),
(32, 205, '1538910573.jpg', '2018-10-07 08:09:33', '2018-10-07 08:09:33'),
(37, 205, '1538910972126.jpg', '2018-10-07 08:16:12', '2018-10-07 08:16:12'),
(38, 205, '1538910973872.jpg', '2018-10-07 08:16:13', '2018-10-07 08:16:13'),
(39, 205, '1538910973842.jpg', '2018-10-07 08:16:13', '2018-10-07 08:16:13'),
(40, 206, '1538911461140.jpg', '2018-10-07 08:24:21', '2018-10-07 08:24:21'),
(41, 206, '1538911462926.jpg', '2018-10-07 08:24:22', '2018-10-07 08:24:22'),
(42, 206, '1538911462277.jpg', '2018-10-07 08:24:22', '2018-10-07 08:24:22'),
(43, 207, '1538912043366.jpg', '2018-10-07 08:34:03', '2018-10-07 08:34:03'),
(44, 207, '1538912191466.jpg', '2018-10-07 08:36:31', '2018-10-07 08:36:31'),
(45, 207, '1538912192217.jpg', '2018-10-07 08:36:32', '2018-10-07 08:36:32');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_09_22_212025_create_articles_table', 2),
(4, '2018_10_04_172105_create_article_images_table', 3),
(5, '2018_10_06_172449_create_article_comments_table', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(7, 'Коритний Антон', 'anton@gmail.com', NULL, '$2y$10$DQC4okBgPOoB1LhkHTZu6.SDOjuy0vOydKnCHsE2Ec/AOGk6pWZai', NULL, '2018-10-07 06:55:00', '2018-10-07 06:55:00');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `article_comments`
--
ALTER TABLE `article_comments`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `article_images`
--
ALTER TABLE `article_images`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=208;

--
-- AUTO_INCREMENT для таблицы `article_comments`
--
ALTER TABLE `article_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT для таблицы `article_images`
--
ALTER TABLE `article_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

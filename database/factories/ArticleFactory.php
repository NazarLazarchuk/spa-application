<?php

use Faker\Generator as Faker;

$factory->define(App\Article::class, function (Faker $faker) {
    return [
        'author_id' => 0,
        'title' => $faker->text(50),
        'body' => $faker ->text(200)
    ];
});
